##
## BUILD image
##
ARG ARCH=amd64
FROM $ARCH/alpine:3 AS build-image

RUN apk --no-cache --no-progress add git go gcc libc-dev aom-dev

RUN mkdir -p /go/src/github.com/Kagami \
  && cd /go/src/github.com/Kagami \
  && git clone https://github.com/Kagami/go-avif.git \
  && GOPATH=/go go mod init \
  && GOPATH=/go go get -v ./...

##
## RUNTIME image
##
ARG ARCH=amd64
FROM $ARCH/alpine:3 AS runtime-image

MAINTAINER Michael Balser <michael@balser.cc>

# args
ARG VCS_REF
ARG BUILD_DATE

# labels
LABEL maintainer="Michael Balser <michael@balser.cc>" \
  org.label-schema.schema-version="1.0" \
  org.label-schema.name="michaelbalser/avif" \
  org.label-schema.description="avif docker image" \
  org.label-schema.version="0.1" \
  org.label-schema.url="https://hub.docker.com/repository/docker/michaelbalser/avif" \
  org.label-schema.vcs-url="https://gitlab.com/mbalser1/avif" \
  org.label-schema.vcs-ref=$VCS_REF \
  org.label-schema.build-date=$BUILD_DATE

# install packages
RUN apk --no-cache --no-progress add tini aom-libs

COPY --from=build-image /go/bin/avif /avif

ENTRYPOINT ["/sbin/tini", "--", "/avif"]
