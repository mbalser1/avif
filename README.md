# avif docker image

## About
- image is always based on latest stable AlpineLinux v3
- automated builds - daily rebuild images
- multi-arch: amd64, arm32v6, arm32v7, arm64

Docker: https://hub.docker.com/repository/docker/michaelbalser/avif

Docker images available for:
- amd64
- arm64v8
- arm32v6
- arm32v7

This image contains an executable compiled from the sourcecode of: https://github.com/Kagami/go-avif

## Multi-arch Image
The below commands reference a
[Docker Manifest List](https://docs.docker.com/engine/reference/commandline/manifest/)
at [`michaelbalser/avif`](https://hub.docker.com/repository/docker/michaelbalser/avif).

Simply running commands using this image will pull
the matching image architecture (e.g. `amd64`, `arm32v6`, `arm32v7`, or `arm64`) based on
the hosts architecture. Hence, if you are on a **Raspberry Pi** the below
commands will work the same as if you were on a traditional `amd64`
desktop/laptop computer.

## Usage

````
# docker run --rm michaelbalser/avif --help
Usage: avif [options] -e src_filename -o dst_filename

AVIF encoder

Options:
  -h, --help                Give this help
  -V, --version             Display version number
  -e <src>, --encode=<src>  Source filename
  -o <dst>, --output=<dst>  Destination filename
  -q <qp>, --quality=<qp>   Compression level (0..63), [default: 25]
  -s <spd>, --speed=<spd>   Compression speed (0..8), [default: 4]
  -t <td>, --threads=<td>   Number of threads (0..64, 0 for all available cores), [default: 0]
  --lossless                Lossless compression (alias for -q 0)
  --best                    Slowest compression method (alias for -s 0)
  --fast                    Fastest compression method (alias for -s 8)
````

The image can be used like any commandline tool:

````
docker run \
  --rm \
  --volume <picture_directory>:/mnt \
  michaelbalser/avif \
  --fast --encode=/mnt/<picture1>.jpg \
  --output=/mnt/<picture1>.avif
````

But be warned - the memory footprint of the avif encoder is...impressively huge.